package de.resolution.atlashttpdumper;

import de.resolution.atlashttpdumper.util.HttpServletRequestWrapperFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.Locale;

/**
 * Implementation of an HTTP servlet {@link Filter} which dumps the contents of an HTTP packet to the logger
 */
public class HttpDumperFilter implements Filter {

    private static final CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();

    private static final String ALREADY_FILTERED = HttpDumperFilter.class.getName() + "_already_filtered";

    /**
     * Class logger.
     */
    private final Logger log = LoggerFactory.getLogger(HttpDumperFilter.class);

    /**
     * Constructor.
     */
    public HttpDumperFilter() {
    }

    /**
     * {@inheritDoc}
     */
    public void init(final FilterConfig filterConfig) {
        log.warn("=====                        Initializing HttpDumper Filter                         =====\n");
        log.warn("=====            NOTE: THIS WILL DUMP CONFIDENTIAL INFORMATION TO THE LOGS          =====\n");
        log.warn("=====  Disable or uninstall the plugin as soon as you're done with your debugging.  =====");
        log.warn("Initializing HttpDumper Filter");
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

    /**
     * General filter function
     * {@inheritDoc}
     */
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (request.getAttribute(ALREADY_FILTERED) != null) {
            chain.doFilter(request, response);
            return;
        }
        request.setAttribute(ALREADY_FILTERED, "yes");


        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("Request is not an instance of HttpServletRequest");
        }

        if (!(response instanceof HttpServletResponse)) {
            throw new ServletException("Response is not an instance of HttpServletResponse");
        }

        // This creates a wrapper which can read out the body and still make it accessible to the downstream handlers
        HttpServletRequest httpServletRequest = HttpServletRequestWrapperFactory.createHttpServletRequestWrapper((HttpServletRequest) request);
        try {
            logRequest(httpServletRequest);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            log.warn("Non-IOException while dumping http request: {}", e.toString());
        }

        // Pass it along
        chain.doFilter(httpServletRequest, response);
    }

    /**
     * This is where the magic happens. Log everything from this app
     * @param request
     * @throws IOException
     */
    public void logRequest(final HttpServletRequest request) throws IOException {
        // Uncomment this to ignore GET requests

//        if (request.getMethod().equalsIgnoreCase("GET")) {
//            return;
//        }

        StringBuilder sb = new StringBuilder();

        sb.append("\n----- REQUEST RECEIVED FROM ");
        sb.append(request.getRemoteAddr());
        sb.append(':');
        sb.append(request.getRemotePort());
        sb.append(" at ");
        sb.append(ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT));
        sb.append('\n');

        sb.append(request.getMethod().toUpperCase(Locale.US));
        sb.append(' ');
        sb.append(request.getRequestURI());

        String queryString = request.getQueryString();
        if (queryString != null && queryString.length() > 0) {
            sb.append('?');
            sb.append(queryString);
        }

        sb.append(' ');
        sb.append(request.getProtocol());

        sb.append('\n');

        // HTTP headers
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            Enumeration<String> headerValues = request.getHeaders(headerName);
            while (headerValues.hasMoreElements()) {
                String headerValue = headerValues.nextElement();
                sb.append(headerName);
                sb.append(": ");
                sb.append(headerValue);
                sb.append('\n');
            }
        }

        sb.append("[java-parsed-content-type]: ");
        sb.append(request.getContentType());
        sb.append('\n');

        if (request.getContentType() != null) {
            sb.append('\n');

            byte[] bodyBytes;
            try {
                if (request instanceof ContentCachingRequestWrapper) {
                    bodyBytes = ((ContentCachingRequestWrapper) request).getContentAsByteArray();
                } else {
                    InputStream is = request.getInputStream();
                    bodyBytes = IOUtils.toByteArray(is);
                }
                // try to decode data as string to see if it is binary
                CharSequence body = decoder.decode(ByteBuffer.wrap(bodyBytes));
                sb.append(body);
            } catch (CharacterCodingException ex) {
                sb.append("[Request body appears to be binary]");
            } catch (IOException e) {
                sb.append("[Request body could not be decoded:");
                sb.append(e.getMessage());
                sb.append(']');
            }
            sb.append("\n");
        }

        sb.append("----- REQUEST END");

        log.warn(sb.toString());
    }

}
