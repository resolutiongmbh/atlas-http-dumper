package de.resolution.atlashttpdumper.util;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

// This whole file is heavily inspired by https://stackoverflow.com/a/54258488/2363038
/**
 * Makes a "copy" of the HttpRequest so the body can be accessed more than 1 time.
 * See : https://stackoverflow.com/questions/44182370/why-do-we-wrap-httpservletrequest-the-api-provides-an-httpservletrequestwrappe/44187955#44187955
 * DOES NOT WORK WITH APPLICATION_FORM_URLENCODED_VALUE
 */
public final class PersistedBodyRequestWrapper extends HttpServletRequestWrapper {

    public static final String ERROR_MSG_CONTENT_TYPE_NOT_SUPPORTED = "ContentType not supported. (ContentType=\"%1$s\")";

    public static final String ERROR_MSG_PERSISTED_BODY_REQUEST_WRAPPER_CONSTRUCTOR_FAILED = "PersistedBodyRequestWrapper constructor FAILED";

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PersistedBodyRequestWrapper.class);

    private final byte[] body;

    private final Map<String, String[]> parameterMap;

//    private final Collection<Part> parts;
//    private final Map<String, Part> partsByName;

    public PersistedBodyRequestWrapper(final HttpServletRequest request) throws IOException, ServletException {
        super(request);

        String contentType = request.getContentType();
        /* Allow everything EXCEPT APPLICATION_FORM_URLENCODED_VALUE */
        if (null != contentType && contentType.equalsIgnoreCase(MediaType.APPLICATION_FORM_URLENCODED_VALUE)) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException(String.format(ERROR_MSG_CONTENT_TYPE_NOT_SUPPORTED, MediaType.APPLICATION_FORM_URLENCODED_VALUE));
            log.error(ERROR_MSG_PERSISTED_BODY_REQUEST_WRAPPER_CONSTRUCTOR_FAILED, illegalArgumentException);
            throw illegalArgumentException;
        }

//        if (contentType != null && contentType.split(";")[0].equalsIgnoreCase(MediaType.MULTIPART_FORM_DATA_VALUE)) {
//            parts = request.getParts();
//            if (parts != null) {
//                partsByName = new HashMap<>(parts.size());
//                for (Part part : parts) {
//                    partsByName.put(part.getName(), part);
//                }
//            } else {
//                partsByName = Collections.emptyMap();
//            }
//        } else {
//            parts = Collections.emptyList();
//            partsByName = Collections.emptyMap();
//        }

        parameterMap = request.getParameterMap();
        this.body = IOUtils.toByteArray(request.getInputStream());
//        log.warn("Non-URL-encoded body length " + this.body.length);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new CustomServletInputStream(this.body);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return this.parameterMap;
    }

//    @Override
//    public Collection<Part> getParts() throws IOException, ServletException {
//        return parts;
//    }
//
//    @Override
//    public Part getPart(String name) throws IOException, ServletException {
//        return partsByName.get(name);
//    }
}
