# HTTP Dumper Plugin for Atlassian 

Dumps all incoming HTTP packets to the logger. It dumps all content (expect binary content) to the logger at the warn level. There is no configuration.

!!! This app is not meant to be used in production! It will dump session cookies and other secrets in the application logs and there is probably a performance penalty involved !!!

Simply install from the [downloads section](https://bitbucket.org/resolutiongmbh/atlas-http-dumper/downloads/) into any Atlassian host app and enjoy. There is one version which logs all request and one which only logs non-GET requests.

Currently, only tested on Confluence 7.14, but it should work on recent versions of Confluence, Jira and Bitbucket as well. This is not tested though.

## Modifying and Building

To build this plugin, install the [Atlassian SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/) on your machine, then navigate a terminal to this repository's directory and run

```bash
atlas-mvn clean package
```

The resulting artifact `jar` file will be located in the `target` directory.

## If you like this app...

...check out our [website](https://resolution.de) or our listings on the [Atlassian Marketplace](https://marketplace.atlassian.com/vendors/1210947/resolution-reichert-network-solutions-gmbh) :)
